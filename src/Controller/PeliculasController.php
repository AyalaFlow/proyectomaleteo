<?php

namespace App\Controller;

use App\Entity\Pelicula;
use App\Form\ArticuloFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PeliculasController extends AbstractController
{

    //ESTA FUNCION NOS PERMITE IMPRIMIR EN PANTALLA EN UNA PLANTILLA TWIG UN INDEX MUY SENCILLO
    //CREADO A PARTIR DE SYMFONY  

    /**
     * @Route("/peliculas", name="peliculas")
     */
     public function inicio()
     {
         return $this->render('peliculas/index.html.twig', [
             'controller_name' => 'PeliculasController',
       ]);
    }



    /**
     * @Route("/peli", name ="listar-pelis")
     */

    public function listarPeliculas(EntityManagerInterface $em){
        $repositorio = $em->getRepository(Pelicula::class);
        $pelicula = $repositorio -> findAll();

        return $this->render(
            'articulo/listado-articulos.html.twig',
            
            [
              'movies' =>  $pelicula 
            ]
        );

        dd($pelicula);
    } 




    //ESTA FUNCION NOS PERMITE INSERTAR DATOS A LA BASE DE DATOS.

     /**
      * @Route("/peliculas/new", name="nueva-pelicula")
      */
    //  public function nuevaPelicula(EntityManagerInterface $em)
    //  {
    //      /*CREAMOS UN OBJETO PELICULA A PINCHO, PARA INTRODUCIR EN LA BASE DE DATOS.*/
    //      $pelicula = new Pelicula();
    //      $pelicula->setTitulo('blow');
    //      $pelicula->setDescripcion('pelicula del narco trafico');

    //     //guardar en bbdd, PERSIST DEL OBJETO PELICULA(ES COMO COGE EL DATO) Y FLUSH INTRODUCE LOS DATOS EN LA BASE DE DATOS.*/

    //      $em->persist($pelicula);
    //      $em->flush();

    //     return new Response('INSERTADO CON EXITO');
    // } 

    // ESTA FUNCION NOS PERMITE LLAMAR A LA CREACION DE UN FORMULARIO, NOS DA 
    // LA RUTA PARA EL ACCESO AL FORMULARIO, A SU VEZ DEBEMOS CREAR OTRA FUNCION QUE NOS PERMITIRA 
    // CREAR LOS ELEMENTOS DEL FORMULARIO.
 


    /**
     * @Route("/form/new", name="nuevo-formulario")
     */

    public function nuevoFormulario(EntityManagerInterface $em, Request $request){

        $peliForm = $this->createForm(ArticuloFormType::class);
    

        //Creamos una variable que crea el formulario cogiendo los campos anteriormente definidos en ArticuloFormType.
        $peliForm->handleRequest($request);


        if($peliForm->isSubmitted() && $peliForm->isValid())
        {

            $pelicula = $peliForm->getData();

            $em->persist($pelicula);
            $em->flush();

            return $this->redirectToRoute("peliculas");
        }



    
        return $this->render("articulo/nuevo-articulo.html.twig",//ESTA PARTE LLAMA AL FICHERO QUE TIENE LA LOGICA PARA CREAR UN FORMULARIO.
                                [
                             "formulario" => $peliForm->createView(),//Esta linea crea el HTML necesario para el formulario, crea el formulario.
                             "nombre" => "Primer Formulario"//Crea un texto 
                                ]
                             );

    }

    



}
