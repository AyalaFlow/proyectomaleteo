<?php

namespace App\Controller;

use App\Emoji\EmojiTranslator;
use App\Form\ArticuloFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController{

    /*ESTA FUNCION NOS PERMITE USAR UNA API EMOJITRANSLATOR QUE NOS TRANSFORMA PALABRAS EN EMOJIS(NO FUNCIONA POR MOTIVOS DE LA IMAGEN INSTALADA DE DOCKER) */

    /**
     * @Route("/EMOJIS", name="homepage")
     */
public function homepage(EmojiTranslator $emojiTranslator){

    $texto = "Esto es un texto de prueba caca";
    $textoConvertido = $emojiTranslator->convert($texto);
    return new Response($textoConvertido);
}


// /*ESTA FUNCION NOS PERMITE LLAMAR A LA CREACION DE UN FORMULARIO, NOS DA 
// LA RUTA PARA EL ACCESO AL FORMULARIO, A SU VEZ DEBEMOS CREAR OTRA FUNCION QUE NOS PERMITIRA 
// CREAR LOS ELEMENTOS DEL FORMULARIO.
//  */


// /**
//  * @Route("/articulo/new", name="nuevo-articulo")
//  */
// public function nuevoArticulo(Request $request){

//     $articuloForm = $this->createForm(ArticuloFormType::class);//Creamos una variable que crea el formulario cogiendo los campos anteriormente definidos en ArticuloFormType.
    
//     $articuloForm->handleRequest($request);


//     if($articuloForm->isSubmitted() && $articuloForm->isValid() ){

//         dd($articuloForm->getData());

//         return $this->redirectToRoute('pelicula');
//     }

    
//     return $this->render("articulo/nuevo-articulo.html.twig",//
//     [
//         "formulario" => $articuloForm->createView(),//Esta linea crea el HTML necesario para el formulario, crea el formulario.
//         "nombre" => "Primer Formulario"//Crea un texto 

//     ]

//   );



// }

}