<?php

    namespace App\Controller;

    use App\Entity\Comentarios;
    use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
    use Symfony\Component\Routing\Annotation\Route;
    use App\Form\FormType;
    use Doctrine\ORM\EntityManagerInterface;
    use Symfony\Component\HttpFoundation\Request;


    class MaleteoController extends AbstractController{

    /**
     * @Route("/", name="homepage")
     */

    public function maleteo(EntityManagerInterface $em, Request $request) {
        $Formu = $this->createForm(FormType::class);
        $Formu->handleRequest($request);

        if($Formu->isSubmitted() && $Formu->isValid()){
            $solicitud = $Formu->getData();
 
                $em->persist($solicitud);
                $em->flush();

            return $this->redirectToRoute('homepage');
                }
 

        return $this->render('maleteo/index.html.twig',
        [
            'DemoForm' => $Formu->createView(),
         // 'opiniones' => $opiniones
        ]);



    }

    
}


?>