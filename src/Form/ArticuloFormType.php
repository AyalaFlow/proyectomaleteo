<?php

namespace App\Form;

use App\Entity\Pelicula;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticuloFormType extends AbstractType{


    public function buildForm(FormBuilderInterface $builder, array $options){

        $builder->add('Titulo');
        $builder->add('Descripcion');
        $builder->add('Genero');

    }public function configureOptions(OptionsResolver $resolver)
    {
        $resolver ->setDefaults([
            'data-class' => Pelicula::class

        ]);
    }

}








?>